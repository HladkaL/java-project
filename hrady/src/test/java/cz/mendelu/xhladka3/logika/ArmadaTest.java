package cz.mendelu.xhladka3.logika;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmadaTest {
    @Test
    public void rozdelArmaduTest_spravny_pocet() {
        Armada armada = new Armada(8,Barva.CERVENA);
        Armada expArmada = new Armada(3,Barva.CERVENA);

        armada.rozdelArmadu(5);

        assertEquals(expArmada.getPocetJednotek(), armada.getPocetJednotek());
    }

    @Test
    public void rozdelArmaduTest_spravny_pocet_return() {
        Armada armada = new Armada(8,Barva.CERVENA);
        Armada expArmada = new Armada(5,Barva.CERVENA);
        Armada oddelena_Armada;

        oddelena_Armada = armada.rozdelArmadu(5);

        assertEquals(expArmada.getPocetJednotek(), oddelena_Armada.getPocetJednotek());
    }



    @Test
    public void rozdelArmaduTest_spatny_pocet(){
        Armada armada = new Armada(8,Barva.CERVENA);
        Armada expArmada = new Armada(8,Barva.CERVENA);

        armada.rozdelArmadu(9);

        assertEquals(expArmada.getPocetJednotek(), armada.getPocetJednotek());
    }

    @Test
    public void rozdelArmaduTest_spatny_pocet_return_null(){
        Armada armada = new Armada(8,Barva.CERVENA);
        Armada oddelena_Armada;

        oddelena_Armada = armada.rozdelArmadu(9);

        assertNull(oddelena_Armada);
    }




    @Test
    public void slucArmaduTest(){
        Armada armada = new Armada(3,Barva.CERVENA);
        Armada armadaSKym = new Armada(3,Barva.CERVENA);
        Armada expArmada = new Armada(6,Barva.CERVENA);

        armada.slucArmadu(armadaSKym);

        assertEquals(expArmada.getPocetJednotek(), armada.getPocetJednotek());
    }



//    @Test
//    public void slucArmaduTestNull(){
//        Armada armada = new Armada(3,Barva.CERVENA);
//        Armada armadaSKym = new Armada(3,Barva.CERVENA);
//        Armada expArmada = new Armada(0,Barva.CERVENA);
//
//        armada.slucArmadu(armadaSKym);
//
//        assertNull(armadaSKym);
//    }


}