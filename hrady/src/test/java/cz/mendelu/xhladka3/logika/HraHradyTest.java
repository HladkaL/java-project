package cz.mendelu.xhladka3.logika;

import org.junit.jupiter.api.Test;


import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HraHradyTest {


    @Test
    void konecHryTest_true() {
        Hrad hrad = new Hrad(2,new Pozice(2,2));
        HraHrady.getHraHrady().pridejHrad(hrad);
        HraHrady.getHraHrady().setPocetObsazenychHradu(1);

        assertTrue( HraHrady.getHraHrady().konecHry());

    }

    @Test
    void konecHryTest_false() {
        Hrad hrad = new Hrad(2,new Pozice(2,2));
        HraHrady.getHraHrady().pridejHrad(hrad);

        assertFalse( HraHrady.getHraHrady().konecHry());

    }
}