package cz.mendelu.xhladka3.logika;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class KostkaTest {

    @Test
    public void getNovaHodnotaTest() {
        int hozenaHodnota = Kostka.getKostka().getNovaHodnota();
        assertTrue(hozenaHodnota >= 1 && hozenaHodnota <= 6);
    }
}