package cz.mendelu.xhladka3.logika;

import java.util.Objects;

public class Armada {
    private int pocetJednotek;
    private Barva vlastnictvi;
    private Policko policko = null;

    /**
     * @version: etapa 3
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Armada armada = (Armada) o;
        return pocetJednotek == armada.pocetJednotek &&
                vlastnictvi == armada.vlastnictvi &&
                Objects.equals(policko, armada.policko);
    }

    /**
     * @version: etapa 3
     */
    @Override
    public int hashCode() {
        return Objects.hash(pocetJednotek, vlastnictvi, policko);
    }

    /**
     * @version: etapa 3
     */
    @Override
    public String toString() {
        return  vlastnictvi +" armada" +
                " ,pocet jednotek: " + pocetJednotek +".";
    }

    public Armada(int pocetJednotek, Barva vlastnictvi) {
        this.pocetJednotek = pocetJednotek;
        this.vlastnictvi = vlastnictvi;
    }

    public void setPocetJednotek(int pocetJednotek) {
        this.pocetJednotek = pocetJednotek;
    }

    public void setVlastnictvi(Barva vlastnictvi) {
        this.vlastnictvi = vlastnictvi;
    }

    public int getPocetJednotek() {
        return pocetJednotek;
    }

    public Barva getVlastnictvi() {
        return vlastnictvi;
    }



    /**
     * @version: etapa 3
     *
     * Metoda rozdelujici armadu na 2 armadz.
     * @param kolikOddelit Celočíselná hodnota udávající počet, koli jednotek oddělit.
     * @return Nový objekt třídy Armada s příslušným počtem jednotek.
     */
    public Armada rozdelArmadu(int kolikOddelit) {
         if (kolikOddelit<this.getPocetJednotek()){
             Armada armada = new Armada(kolikOddelit,this.getVlastnictvi());
             this.setPocetJednotek(getPocetJednotek()-kolikOddelit);
             return armada;
         }
         else return null;


    }


    /**
     * @version: etapa 3
     *
     * Metoda slucující 2 armády stejného vlastnictví dohromady.
     * @param sKym Armáda se kterou se sloučí.
     */
    public void slucArmadu(Armada sKym) {
        if (this.getVlastnictvi()==sKym.getVlastnictvi()){
        this.setPocetJednotek(sKym.getPocetJednotek()+this.getPocetJednotek());
        sKym.setPocetJednotek(0);
        } else {
            System.out.println("Nelze sloucit.");
        }
    }

    public Policko getPolicko() {
        return policko;
    }

    public void setPolicko(Policko policko) {
        this.policko = policko;
    }
}
