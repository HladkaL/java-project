package cz.mendelu.xhladka3.logika;

import java.util.*;

/**
 * V ramci hry je tato trida jedinacek.
 * @author Lenka Hladká
 */

public class HraHrady {
    private Mapa mapa;
    private Set<Hrac> hraci;
    private List<Hrad> hrady;
    private boolean konecHry = false;
    private int pocetObsazenychHradu;

    private static final HraHrady HRA_HRADY = new HraHrady();

    /**
     * @version: etapa 3
     * ↓
     */
    private HraHrady() {
        hrady = new ArrayList<>();
        hraci = new HashSet<>();
        mapa = Mapa.getMAPA();
        pocetObsazenychHradu=0;
    }

    public static HraHrady getHraHrady() {
        return HRA_HRADY;
    }

    public Set<Hrac> getHraci() {
        return Collections.unmodifiableSet(hraci);
    }

    public List<Hrad> getHrady() {
        return hrady;
    }

    public void pridejHrace(Hrac hrac) {
        hraci.add(hrac);
    }

    public void pridejHrad(Hrad hrad) {
        hrady.add(hrad);
    }

    public void odeberHrad(Hrad hrad){
        hrady.remove(hrad);
    }


    /**
     * ↑
     * @version: etapa 3
     */

    public int getPocetObsazenychHradu() {
        return pocetObsazenychHradu;
    }

    public Mapa getMapa() {
        return mapa;
    }

    public void startHry(int kolikHacu) {
        if (kolikHacu >0 && kolikHacu>7){
            for (int i = 0; i <kolikHacu ; i++) {
                pridejHrace(new Hrac(Barva.getBarva(i)));

            }

        } else System.out.println("Nepripustny pocet hracu! (1-7 hracu)");
    }


    /**
     *@version: etapa 3
     *
     * Metoda zjistujici, jestli je konec hry, coz nastane pokud uz jsou obsazeny vsechny hrady.
     * @return Vraci 0 kdyz jeste neni konec hry nebo 1 kdyz je konec hry.
     */
    public boolean konecHry() {
        if(pocetObsazenychHradu==hrady.size()){
            return true;
        }
        else return false;

    }

    public void provedHerniKolo() {
//
//        int pocetObsazenychHradu = 0;
//        for (Hrac hr : hraci) {
//            pocetObsazenychHradu += hr.getHradyHrace().size();
//        }
//
//        if(pocetObsazenychHradu == mapa.getHrady().size()){
//                return true;
//        }else return false;
    }


    public void setPocetObsazenychHradu(int pocetObsazenychHradu) {
        this.pocetObsazenychHradu = pocetObsazenychHradu;
    }
}
