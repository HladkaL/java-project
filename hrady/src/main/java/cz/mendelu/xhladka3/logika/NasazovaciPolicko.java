package cz.mendelu.xhladka3.logika;

import java.util.HashSet;
import java.util.Set;

/**
 * Objekt reprezentuje 1 nasazovaci policko na mape.
 */
public class NasazovaciPolicko extends Policko {
    public enum CiselneOznaceni{
        JEDNA,
        DVA,
        TRI,
        CTYRI,
        PET,
        SEST
    }
    private CiselneOznaceni ciselneOznaceni;
    private  Set<Armada> mnozinaArmad = new HashSet<>();

    public NasazovaciPolicko(Pozice pozice,CiselneOznaceni ciselneOznaceni) {
        super(pozice);
        this.ciselneOznaceni = ciselneOznaceni;
    }

    public CiselneOznaceni getCiselneOznaceni() {
        return ciselneOznaceni;
    }

    public void setCiselneOznaceni(CiselneOznaceni ciselneOznaceni) {
        this.ciselneOznaceni = ciselneOznaceni;
    }

    public Set<Armada> getMnozinaArmad() {
        return mnozinaArmad;
    }

    public void setMnozinaArmad(Set<Armada> mnozinaArmad) {
        this.mnozinaArmad = mnozinaArmad;
    }
}
