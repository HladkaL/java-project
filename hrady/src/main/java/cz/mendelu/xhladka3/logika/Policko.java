package cz.mendelu.xhladka3.logika;

public abstract class Policko {
    protected Pozice pozice;

    public Policko(Pozice pozice) {
        this.pozice = pozice;
    }

    public Pozice getPozice() {
        return pozice;
    }

    public void setPozice(Pozice pozice) {
        this.pozice = pozice;
    }
}
