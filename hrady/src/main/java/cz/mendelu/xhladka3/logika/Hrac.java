package cz.mendelu.xhladka3.logika;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Objekt reprezentujici hrace v jedne hre.
 */

public class Hrac {

    private Barva barva;
    private List<Hrad> hradyHrace;
    private List<Armada> armadyHrace;

    public Hrac(Barva barva) {
        this.barva = barva;
        hradyHrace = new ArrayList<>();
        armadyHrace = new ArrayList<>();
    }

    /**
     * @version: etapa 3
     */
    @Override
    public String toString() {
        return "Hrac, barva:" +barva + ", pocet hradu: " + hradyHrace.size()
                +"pocet armad: " + armadyHrace.size();
    }

    /**
     * @version: etapa 3
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hrac hrac = (Hrac) o;
        return toString() == hrac.toString();
    }

    /**
     * @version: etapa 3
     */
    @Override
    public int hashCode() {
        return Objects.hash(toString());
    }

    public Barva getBarva() {
        return barva;
    }

    public List<Hrad> getHradyHrace() {
        return hradyHrace;
    }

    public List<Armada> getArmadyHrace() {
        return armadyHrace;
    }

    public void odehrajTah() {
    }

    private int hodKostkou(){
        return Kostka.getKostka().getNovaHodnota();
    }

    private void nasadArmadu(int kolikNaKoatce) {
    }

    private void bojuj(Armada sKym) {
    }

    public void presunArmadu(Armada kterouArmadu, Policko kam) {
    }

    public void rozdelAPresunArmadu(Armada kerouArmadu, Policko kam) {
    }

    private void dobyjHrad(Hrad ktery) {
    }


}
