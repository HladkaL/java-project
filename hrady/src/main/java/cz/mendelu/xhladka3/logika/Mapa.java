package cz.mendelu.xhladka3.logika;

import java.util.ArrayList;
import java.util.List;

/**
 * Objekt repreyentuje mapu v jedne hre. Mapa je jedinacek.
 */
public class Mapa {

    private final int SIRKA=9;
    private final int VYSKA=6;
    private static final Mapa MAPA = new Mapa();
    private List<List<Policko>> herniPole;


    private Mapa() {

       herniPole = new ArrayList<>();
        for (int i = 0; i < SIRKA; i++) {
            herniPole.add(new ArrayList<>());
            for (int j = 0; j < VYSKA; j++) {
                herniPole.get(i).add(new PolickoPohybu(new Pozice(i,j)));
            }
        }
        //NASAZOVACI
        herniPole.get(0).remove(0);
        herniPole.get(0).add(0, new NasazovaciPolicko(
                new Pozice(0,0),NasazovaciPolicko.CiselneOznaceni.SEST));

        herniPole.get(4).remove(0);
        herniPole.get(4).add(0, new NasazovaciPolicko(
                new Pozice(4,0),NasazovaciPolicko.CiselneOznaceni.PET));

        herniPole.get(8).remove(0);
        herniPole.get(8).add(0, new NasazovaciPolicko(
                new Pozice(8,0),NasazovaciPolicko.CiselneOznaceni.CTYRI));

        herniPole.get(0).remove(5);
        herniPole.get(0).add(5, new NasazovaciPolicko(
                new Pozice(0,5),NasazovaciPolicko.CiselneOznaceni.JEDNA));

        herniPole.get(4).remove(5);
        herniPole.get(4).add(5, new NasazovaciPolicko(
                new Pozice(4,5),NasazovaciPolicko.CiselneOznaceni.DVA));

        herniPole.get(8).remove(5);
        herniPole.get(8).add(5, new NasazovaciPolicko(
                new Pozice(8,5),NasazovaciPolicko.CiselneOznaceni.TRI));

        //HRADY
        herniPole.get(2).remove(2);
        herniPole.get(2).add(2, new Hrad(
                new Pozice(2,2),1));

        herniPole.get(5).remove(2);
        herniPole.get(5).add(2, new Hrad(
                new Pozice(5,2),2));

    }

    public static Mapa getMAPA() {
        return MAPA;
    }


    public void printMapa(){
        System.out.println("__________________________________________________________________");
        System.out.println("Vykresleni mapy: ");
        for (int i = 0; i < herniPole.size(); i++) {
            System.out.print("  " + (i) +"  |");
        }
        System.out.println();

        for (int i = 0; i < herniPole.size(); i++) {
            System.out.print( "  " + (i+1) + "  |");
            for (int j = 0; j < herniPole.get(i).size(); j++) {
                if (herniPole.get(i).get(j) == null) {
                    System.out.print("  ??  ");
                } else {
                    System.out.print(" neco ");

                }
            }
            System.out.println();

        }


    }

    public List<List<Policko>> getHerniPole() {
        return herniPole;
    }
}
