package cz.mendelu.xhladka3.logika;

public class Pozice {
    private int x;
    private int y;


    public Pozice(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }



   public boolean jePoziceSousedni(Pozice pozice) {

       return Math.abs(this.getX()  - pozice.getX()) < 2 &&
                Math.abs(this.getY()  - pozice.getY()) < 2;
   }
}
