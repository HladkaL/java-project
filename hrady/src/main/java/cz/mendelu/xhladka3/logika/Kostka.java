package cz.mendelu.xhladka3.logika;
import java.util.Random;

/**
 * Třída Kostka.
 *
 */

public class Kostka {
    private static final int ROZSAH_KOSTKY = 6;
    private int hodnota;

    private static final Kostka KOSTKA = new Kostka();

    public static Kostka getKostka(){
        return KOSTKA;
    }

    public int getHodnota() {
        return hodnota;
    }


    /**
     * @version: etapa 3
     *
     * Metoda, která vrací náhodné číslo v rozmezí 1 až 6.
     * @return celočíselná náhodná hodnota.
     */
    public int getNovaHodnota() {
        Random random = new Random();
        hodnota = random.nextInt(ROZSAH_KOSTKY)+1;
        return hodnota;
    }


}
