package cz.mendelu.xhladka3.logika;

/**
 * Trida reprezentuje policko, na kterem je hrad.
 */
public class Hrad extends Policko {
    private Armada armadaVHradu;
    private Barva vlastnictvi;
    private int urovenHradu;

    public Hrad(Pozice pozice, int urovenHradu) {
        super(pozice);
        this.urovenHradu = urovenHradu;

    }

    public Armada getArmadaVHradu() {
        return armadaVHradu;
    }

    public void setArmadaVHradu(Armada armadaVHradu) {

        this.armadaVHradu = armadaVHradu;
    }

    public Barva getVlastnictvi() {

        return vlastnictvi;
    }

    public void setVlastnictvi(Barva vlastnictvi) {
        this.vlastnictvi = vlastnictvi;
    }

    public int getUrovenHradu() {
        return urovenHradu;
    }

    public void setUrovenHradu(int urovenHradu) {
        this.urovenHradu = urovenHradu;
    }

}
