package cz.mendelu.xhladka3.logika;

public enum Barva {

        CERVENA,
        MODRA,
        ZELENA,
        ZLUTA,
        BILA,
        LOSOSOVA,
        CERNA;



        public static Barva getBarva(int i){
                switch (i){
                        case 1 : return CERVENA;
                        case 2 : return MODRA;
                        case 3 : return ZELENA;
                        case 4 : return ZLUTA;
                        case 5 : return BILA;
                        case 6 : return LOSOSOVA;
                        case 7 : return CERNA;
                        default: return null;
                }


        }

}
