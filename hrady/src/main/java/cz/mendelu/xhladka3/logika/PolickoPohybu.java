package cz.mendelu.xhladka3.logika;

/**
 * Objekt reprezentuje 1 policko na mape.
 */

public class PolickoPohybu extends Policko{
    private Hrac hrac = null;

    public PolickoPohybu(Pozice pozice) {
        super(pozice);

    }

    public Hrac getHrac() {
        return hrac;
    }

    public void setHrac(Hrac hrac) {
        this.hrac = hrac;
    }
}
