package cz.mendelu.xhladka3.grafika;

import cz.mendelu.xhladka3.logika.Armada;
import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.MouseInfo;

public class ArmadaActor extends Actor {

    private static ArmadaActor selectedArmadaActor;

    private static final String ARMADA_IMG = "cervena.png";
    private static final String SELECTED_ARMADA_IMG = "cervenasel.png";

    private Armada armada;

    public ArmadaActor(Armada armada) {
        this.setImage(ARMADA_IMG);
        this.getImage().scale(50,50);
        this.armada=armada;
    }

    @Override
    public void act() {
        MouseInfo mouseInfo = Greenfoot.getMouseInfo();
        if (mouseInfo != null) {
            Actor actor = mouseInfo.getActor();
            if (actor instanceof ArmadaActor) {
                ArmadaActor armadaActor = (ArmadaActor) actor;
                if (isLeftClickOnThisActor(mouseInfo, armadaActor)) {


                    this.setSelected();
                    System.out.println("Selected");
                }
            } else if (actor instanceof PolickoActor) {

                if (isLeftClick(mouseInfo) && selectedArmadaActor == this) {
                    this.presunArmaduNaPolicko((PolickoActor) actor);
                }
            }
        }
    }

    public Armada getArmada() {
        return armada;
    }

    private void setSelected() {
        // Remove selection from previous actor
        if (selectedArmadaActor != null) {
            selectedArmadaActor.setImage(ARMADA_IMG);
        }

        // Change selection of actor
        selectedArmadaActor = this;
        selectedArmadaActor.setImage(SELECTED_ARMADA_IMG);
        this.getImage().scale(50,50);

    }

    private boolean isLeftClickOnThisActor(MouseInfo mouseInfo, ArmadaActor armadaActor) {
        return armadaActor == this &&
                mouseInfo.getButton() == 1 &&
                mouseInfo.getClickCount() == 0;
    }

    private boolean isRightClickOnThisActor(MouseInfo mouseInfo, ArmadaActor armadaActor) {
        return armadaActor == this && mouseInfo.getButton() == 3 && mouseInfo.getClickCount() == 0;
    }

    private boolean isLeftClick(MouseInfo mouseInfo) {

        return mouseInfo.getButton() == 1 &&
                mouseInfo.getClickCount() == 0;
    }


    private void presunArmaduNaPolicko(PolickoActor polickoActor) {

        if (this.getArmada().getPolicko().getPozice()
                .jePoziceSousedni(
                        polickoActor.getPolickoPohybu().getPozice())) {

            setLocation(polickoActor.getX(), polickoActor.getY());
            this.getArmada().setPolicko(polickoActor.getPolickoPohybu());
            System.out.println("Moving to location " +
                    "X: " + polickoActor.getX() + ", " +
                    "Y: " + polickoActor.getY());
        } else {
            System.out.println("Neni sousedni");
        }



    }


}
