package cz.mendelu.xhladka3.grafika;

import cz.mendelu.xhladka3.logika.Armada;
import cz.mendelu.xhladka3.logika.Hrad;
import greenfoot.Actor;

public class HradActor extends Actor {
    private static final String HRAD1_IMG = "hr.png";
    private static final String HRAD2_IMG = "hr2.png";


    private Hrad hrad;
    private Armada armada;


    public HradActor(Hrad hrad) {
        this.hrad=hrad;
        switch (hrad.getUrovenHradu()){
            case 1: setImage(HRAD1_IMG);
                break;
            case 2: setImage(HRAD2_IMG);
                break;
        }

        this.getImage().scale(60,60);

    }

}
