package cz.mendelu.xhladka3.grafika;

import cz.mendelu.xhladka3.logika.*;
import greenfoot.World;

import java.util.ArrayList;
import java.util.List;

public class HerniDeska extends World {

    private static final int WORLD_WIDTH = 800;
    private static final int WORLD_HEIGHT = 600;
    private static final int CELL_SIZE = 1;
    public static final int DISTANCE_BETWEEN = 80;
    public static final int SHIFT_X = 80;
    public static final int SHIFT_Y = 40;

    private List<NasazovaciPolickoActor> nasazovaciPolickoActorList = new ArrayList<>();

    public HerniDeska() {
        super(WORLD_WIDTH, WORLD_HEIGHT, CELL_SIZE);
        this.setBackground("pozadi.png");
        this.getBackground().scale(1000,800);
        setPaintOrder(ArmadaActor.class, PolickoActor.class);


        vykresliPolicka();

        // vyvtvorit novou armadu a nasadit na prvni nasaz. policko
        Armada testArmada = new Armada(1,Barva.CERVENA);
        NasazovaciPolickoActor nasazovaciPolickoActor1 = nasazovaciPolickoActorList.get(0);
        testArmada.setPolicko(nasazovaciPolickoActor1.getNasazovaciPolicko());
        addObject(new ArmadaActor(testArmada),
                nasazovaciPolickoActor1.getX(),
                nasazovaciPolickoActor1.getY());

    }

    private void vykresliPolicka(){

        HraHrady hraHrady = HraHrady.getHraHrady();
        Mapa mapa = hraHrady.getMapa();

        for (List<Policko>  listPolicek : hraHrady.getMapa().getHerniPole()){
            for (Policko policko : listPolicek) {

                if (policko instanceof PolickoPohybu) {

                    PolickoPohybu polickoPohybu = (PolickoPohybu) policko;
                    addObject(new PolickoActor(polickoPohybu),
                            (policko.getPozice().getX()*DISTANCE_BETWEEN) + SHIFT_X,
                            (policko.getPozice().getY()*DISTANCE_BETWEEN) + SHIFT_Y);
                }

                if (policko instanceof NasazovaciPolicko) {

                    NasazovaciPolicko nasazovaciPolicko = (NasazovaciPolicko) policko;

                    NasazovaciPolickoActor nasazovaciPolickoActor
                            = new NasazovaciPolickoActor(nasazovaciPolicko);

                    addObject(nasazovaciPolickoActor,
                            (nasazovaciPolicko.getPozice().getX()*DISTANCE_BETWEEN) + SHIFT_X,
                            (nasazovaciPolicko.getPozice().getY()*DISTANCE_BETWEEN) + SHIFT_Y);

                    nasazovaciPolickoActorList.add(nasazovaciPolickoActor);

                }

                if (policko instanceof Hrad){

                    Hrad hrad = (Hrad) policko;

                    addObject(new HradActor(hrad),
                        (hrad.getPozice().getX()*DISTANCE_BETWEEN) + SHIFT_X,
                        (hrad.getPozice().getY()*DISTANCE_BETWEEN) + SHIFT_Y);
                }

            }

        }

    }
}
