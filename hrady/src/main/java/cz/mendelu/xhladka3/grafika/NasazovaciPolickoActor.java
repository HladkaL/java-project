package cz.mendelu.xhladka3.grafika;

import cz.mendelu.xhladka3.logika.NasazovaciPolicko;
import greenfoot.Actor;

public class NasazovaciPolickoActor extends Actor {

    private static final String NASAZOVACI1_IMG = "1.png";
    private static final String NASAZOVACI2_IMG = "2.png";
    private static final String NASAZOVACI3_IMG = "3.png";
    private static final String NASAZOVACI4_IMG = "4.png";
    private static final String NASAZOVACI5_IMG = "5.png";
    private static final String NASAZOVACI6_IMG = "6.png";

    private NasazovaciPolicko nasazovaciPolicko;

    public NasazovaciPolickoActor(NasazovaciPolicko nasazovaciPolicko) {
        this.nasazovaciPolicko=nasazovaciPolicko;
        switch (nasazovaciPolicko.getCiselneOznaceni() ){
            case JEDNA: setImage(NASAZOVACI1_IMG);
                break;
            case DVA: setImage(NASAZOVACI2_IMG);
                break;
            case TRI: setImage(NASAZOVACI3_IMG);
                break;
            case CTYRI: setImage(NASAZOVACI4_IMG);
                break;
            case PET: setImage(NASAZOVACI5_IMG);
                break;
            case SEST: setImage(NASAZOVACI6_IMG);
                break;
        }
        this.getImage().scale(55,60);
    }

    public NasazovaciPolicko getNasazovaciPolicko() {
        return nasazovaciPolicko;
    }

    public void setNasazovaciPolicko(NasazovaciPolicko nasazovaciPolicko) {
        this.nasazovaciPolicko = nasazovaciPolicko;
    }
}
