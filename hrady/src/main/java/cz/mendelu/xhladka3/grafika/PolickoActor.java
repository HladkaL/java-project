package cz.mendelu.xhladka3.grafika;

import cz.mendelu.xhladka3.logika.PolickoPohybu;
import greenfoot.Actor;

public class PolickoActor extends Actor {

    private static final String POLICKO_IMG = "polickoPohybu.png";

    private PolickoPohybu polickoPohybu;

    public PolickoActor(PolickoPohybu polickoPohybu) {
        this.setImage(POLICKO_IMG);
        this.getImage().scale(60,60);
        this.polickoPohybu = polickoPohybu;
    }

    public PolickoPohybu getPolickoPohybu() {
        return polickoPohybu;
    }

    public void setPolickoPohybu(PolickoPohybu polickoPohybu) {
        this.polickoPohybu = polickoPohybu;
    }
}
