import bh.greenfoot.runner.GreenfootRunner;
import cz.mendelu.xhladka3.grafika.HerniDeska;

/**
 * A sample runner for a greenfoot project.
 */
public class HerniSpoustec extends GreenfootRunner {
    static {
        // Bootstrap the runner class.
        bootstrap(HerniSpoustec.class,
                // Prepare the configuration for the runner based on the world class
                Configuration.forWorld(HerniDeska.class)
                        .projectName("Hrady")
        );
    }
}
